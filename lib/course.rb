class Course
  attr_accessor :name, :department, :credits, :days, :time_block

  def initialize(name, department, credits, days=nil, time_block=nil )
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end

   def students
      @students
   end

    def add_student(student)
      student.enroll(self)
    end

    def conflicts_with?(course)
       self.time_block >= course.time_block && days.any? { |day| course.days.include?(day)}
    end


end

# let(:ruby) { Course.new("Ruby 101", "CS", 4, [:mon, :tue, :wed], 1) }
# let(:python) { Course.new("Python", "CS", 4, [:wed, :thurs, :fri], 1) }
# let(:drama) { Course.new("Drama", "Theatre", 2, [:mon, :wed, :fri], 5) }




# ## And some extras:
# * Each course should also take a set of days of the week (`:mon`,
#   `:tue`, ...), plus a time block (assume each day is broken into 8
#   consecutive time blocks). So a course could meet
#   `[:mon, :wed, :fri]` during block #1.

# * Write a method `Course#conflicts_with?` which takes a second
#   `Course` and returns true if they conflict.
# * Update `Student#enroll` so that you raise an error if a `Student`
#   enrolls in a new course that conflicts with an existing course time.
#     * May want to write a `Student#has_conflict?` method to help.


# bundle exec rspec
