class Student
  attr_accessor :first_name, :last_name
  def initialize(first_name, last_name)
     @first_name = first_name
     @last_name = last_name
     @courses = []
   end
   def name
      "#{first_name} #{last_name}"
   end

   def courses
     @courses
   end

   def enroll(course)
       raise "This course conflicts!" if @courses.any?{|cur_course| course.conflicts_with?(cur_course)}

     unless @courses.include?(course)
       @courses << course
       course.students << self
     end
   end

   def course_load
     hash = Hash.new(0)
     @courses.each do |course|
       hash[course.department] += course.credits
     end
     hash
   end
end





# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.
